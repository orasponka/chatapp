#!/usr/bin/env bash

ip=$(ip route get 1.2.3.4 | awk '{printf $7}')

sed -i "s/192.168.68.196/$ip/ig" data/data.json
sed -i "s/<ip address placeholder>/$ip/ig" public/*.js
sed -i "s/<ip address placeholder>/$ip/ig" public/*/*.js
sed -i "s/<ip address placeholder>/$ip/ig" views/*.ejs

npm i ejs socket.io express cors http path fs uuid
