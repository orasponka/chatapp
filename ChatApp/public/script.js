const socket = io('http://<ip address placeholder>')
const messageContainer = document.getElementById('message-container')
const roomContainer = document.getElementById('room-container')
const messageForm = document.getElementById('send-container')
const messageInput = document.getElementById('message-input')
const messageType = document.getElementsByName('radio')

if(messageForm != null){
    const today = new Date();
    const hours = today.getHours()
    var minutes = (() => {
        var returnValue
        if(today.getMinutes() < 10){
            returnValue = '0' + today.getMinutes() 
        } else {
            returnValue = today.getMinutes()
        }

        return returnValue;
    })
    const aika = ' KLO: ' + hours + '.' + minutes()
    const name = String(window.prompt('Mikä nimesi on? (Tätä kysytyään uudestaan jos joku bannataan)', 'Anonyymi'))
    appendLink('<a href="/">Tästä takaisin valikkoon</a>', "#CCC")
    appendMessage('Sinä liityit ', '<span>' + aika + '</span>', "#CCC")
    socket.emit('new-user', roomName, name, aika)

    messageForm.addEventListener('submit', e => {
        const today = new Date();
        const hours = today.getHours()
        const minutes = today.getMinutes()
        const aika = ' KLO: ' + hours + '.' + minutes 
        e.preventDefault()
        const message = messageInput.value
        appendMessage('Sinä: ' + message,'<span>' + aika + '</span>', "#CCC")
        socket.emit('send-chat-message', roomName, message, aika)
        messageInput.value = '';
    })
}

socket.on('reload', (rooms) => {
    if(roomContainer != undefined || roomContainer != null){
    const  allRooms = Object.keys(rooms)
    setTimeout(onTimeout, 1000)
    function onTimeout(){
    for(i = 0; i < allRooms.length; i++){
        var element = document.getElementById(allRooms[i]);
        var määrä = Object.keys(rooms[allRooms[i]].users).length;
        console.log(rooms[allRooms[i]].users)
        console.log(määrä);
       element.innerText ='Paikalla ' + määrä;
    }
    }
    }
}) 

socket.on('room-created', (room, usersNum, creatorIP) => {
  const roomElement = document.createElement('div')
  roomElement.innerText = room
  const roomLink = document.createElement('a')
  roomLink.href = '/room/' + room
  roomLink.innerText = "Liity"
  const roomUsers = document.createElement('p')
  roomUsers.innerText = 'Paikalla ' + usersNum
  roomUsers.setAttribute('id', room)
  roomUsers.style.fontSize = "17px"
  roomUsers.style.fontWeight = "850"
  const creator = document.createElement('p')
  creator.setAttribute('id', "creator-" + room)
  creator.setAttribute('class', 'creator')
  creator.style.bottom = "13px"
  creator.style.position = "relative"
  creator.style.fontSize = "17px"
  creator.style.fontWeight = "850"
  creator.innerText = "Tekijä: " + creatorIP
  const hr = document.createElement('hr')
  hr.style.width = "20rem"
  roomContainer.append(roomElement)
  roomContainer.append(roomLink)
  roomContainer.append(roomUsers)
  roomContainer.append(creator)
  roomContainer.append(hr)
})

socket.on('chat-message', data => {
    appendMessage(data.name + ': ' + data.message, '<span>' + data.aika + '</span>', "#FFF")
})

socket.on('user-connected', data => {
    appendMessage(data.name + ' liittyi', '<span>' + aika + '</span>', "#FFF")
})

socket.on('user-disconnected', data => {
    appendMessage(data.name + ' poistui', '<span>' + data.aika + '</span>', "#FFF")
})

socket.on('redirect', () => {
    window.location.reload()
})

socket.on('leave', () => {
    window.location = "/home"
})

function appendMessage(message, aika, color) {
    const messageElement = document.createElement('div');
    messageElement.style.backgroundColor = color;
    messageElement.innerText = message
    messageElement.innerHTML += aika
    messageContainer.append(messageElement)
    Scroll();
}

function appendLink(message, color) {
    const messageElement = document.createElement('div');
    messageElement.style.backgroundColor = color;
    messageElement.innerHTML = message
    messageContainer.append(messageElement)
    Scroll();
}

function Scroll() {
    var elem = document.getElementById('div');
    elem.scrollTop = elem.scrollHeight;
}
