/* Määritetään kaikki tärkeimmät jutut */
const express = require("express")
const app = express()
const server = require('http').Server(app)
server.listen(80, '0.0.0.0')/* Tässä määritellään mitä portia serveri kuuntelee */
const io = require('socket.io')(server)
const path = require('path')
const fs = require('fs')/* filesystem jolla voi kirjoittaa tiedostoja ja lukea niitä */
const cors = require('cors')
const uuid = require('uuid')

/* Laitan express js asetukset valmiiksi */
app.set('views', './views') /* Määrittää missä kansiossa on kaikki näkymät */
app.set('view engine', 'ejs')/* Valitsee view enginen eli tavan jolla näkymät on tehty esim html */
app.use(express.static('public'))/* Valitsee hakemiston kaikelle joita näkymät tarvii. Esim style.css tiedostot */
app.use(express.urlencoded({ extended: true }))
app.use(
    cors({
        origin: "*",
    })
)

/* Lukee tarvittavat .json tiedostot joissain on tallennettua dataa */
var data = fs.readFileSync(path.resolve(__dirname, 'data/data.json'));
var automations = fs.readFileSync(path.resolve(__dirname, 'data/automations.json'));
var savedContacts = fs.readFileSync(path.resolve(__dirname, 'data/contacts.json'));
var deletedMessagesJSON = fs.readFileSync(path.resolve(__dirname, 'data/deletedMessages.json'));
var deletedRoomsJSON = fs.readFileSync(path.resolve(__dirname, 'data/deletedRooms.json'));
var botJSON = fs.readFileSync(path.resolve(__dirname, 'data/bot.json'));

/* Määritän tärkeimmät listat */
var rooms = JSON.parse(data).rooms
var privateRooms = JSON.parse(data).privateRooms /* Lista prvivaatti huoneille */
var ipBans = JSON.parse(data).ipBans/* Banni lista */
var ipBanReasons = JSON.parse(data).ipBanReasons/* Bannien syy lista */
var savedInfo = JSON.parse(data).savedInfo/* Lista johon tallennetaan kaikkea joka käyyttäjästä esim nimimerkit ja mitä käyttäjä on tehnyt ja milloin */
var adminIps = JSON.parse(data).adminIps/* Lista valmiiksi määritetyista admin käyttäjistä. */
var botAutomations1Min = JSON.parse(automations).botAutomations1Min/* Lista 1min autimaatioille */
var botAutomations30s = JSON.parse(automations).botAutomations30s/* Lista 30s automaatioille */
var botAutomations5Min = JSON.parse(automations).botAutomations5Min/* Lista 5min autimaatioille */
var botAutomations15s = JSON.parse(automations).botAutomations15s/* Lista 15s automaatioille */
var botAutomationsCustom = JSON.parse(automations).botAutomationsCustom
var contacts = JSON.parse(savedContacts).contacts/* Lista yhteyden otoille */
var deletedMessages = JSON.parse(deletedMessagesJSON).deletedMessages/* Lista poistetuista viesteistä */
var deletedRooms = JSON.parse(deletedRoomsJSON).deletedRooms
var botData = JSON.parse(botJSON).bot

/* Seuraavat kaksi Object.keys juttua poistaa käyttjät listoista jos niitä on jäänyt kun serveri viimeksi sammutettiin */
Object.keys(rooms).forEach(room => {/* Yksinkertaisesti käy huoneet läpi */
    rooms[room].users = {}
    rooms[room].ips = {}
})

Object.keys(privateRooms).forEach(room => {
    privateRooms[room].users = {}
    privateRooms[room].ips = {}
})

/* Kaikki serverin sivut ja mitä niissä tapahtuu */
app.get('/admin', (req,res) => {/* request admin sivulle */
    if(!booleanIpBanned(req.connection.remoteAddress)){
    if(savedInfo[req.connection.remoteAddress] != null){
    if(savedInfo[req.connection.remoteAddress].isAdmin){
    res.render('admin')
    } else {
        res.redirect('/')
    }
    } else {
        res.redirect('/')
    }
    } else {
        res.redirect('/')
    }
})

app.post('/cleardata', (req,res) => {
    if(req.body.data != undefined){
    delete savedInfo[res.connection.remoteAddress]
    }
    if(req.body.rooms != undefined){
    deleteRoomsCreatedByUser(res.connection.remoteAddress)
    }
    if(req.body.messages != undefined){
    deleteUserMessages(res.connection.remoteAddress)
    }
    writeData()
    return res.redirect('/home')
})

app.get('/contact', (req, res) => {
    /* res.render lataa sivulle jonkun tiedoston */
    res.render('contact')
})

app.post('/send', (req, res) => {
    savedInfo[req.connection.remoteAddress].postRequests.push('/send')/* Tallentaa että käyttäjä on tehnyt /send post requestin */
    writeData()
    var contact = { name : req.body.nimi, subject : req.body.aihe, message : req.body.message, senderContact : req.body.yhteys, senderIP : req.connection.remoteAddress }
    contacts.push(contact)
    writeContacts()
    res.redirect('/')/* res.redirect vaihtaa sivua */
})

app.get('/home', (req, res) => {
    /* Tälläinen "booleanIpBanned" tarkistaa on ip bannattu. Boolean tarkoittaa että se antaa vastauksen false tai true muodossa */
    if(booleanIpBanned(req.connection.remoteAddress)){  /* findBanReason etsii bannin syyn pelkän IPn avulla */
        return res.render('ban', { Syy : ipBanReasons[findBanReason(req.connection.remoteAddress)] })
    } else {
    res.render('index', { rooms: rooms })/* Tässä komennon rakenne -> res.render('mikä näkymä', { text : 'tässä määritellään muuttujia sivulle'}) */
    }
})

app.get('/data', (req, res) => { /* /data sivulla voit tarkastella sinusta kerättyjä tietoja */
    if(savedInfo[req.connection.remoteAddress] == null){ /* Jos sinusta ei ole kerätty tietoja eli tietosi on null sinut viedään takaisin etusivulle */
        res.redirect('/home')
    } else {
    res.render('data', { data : savedInfo[req.connection.remoteAddress], ip : req.connection.remoteAddress })
    }
})

app.post('/unadmin', (req, res) => {
    savedInfo[req.connection.remoteAddress].postRequests.push('/unadmin')
    if(booleanIpBanned(req.connection.remoteAddress)){
        writeData()
        return res.render('ban', { Syy : ipBanReasons[findBanReason(req.connection.remoteAddress)] })
    } else {
    if(req.body.nimi == "root" && req.body.salasana == "root321"){/* Jos oot laittanut käyttäjänimen ja salasan oikein saat tehdä asian */
    if(req.body.name != req.connection.remoteAddress){
        savedInfo[req.connection.remoteAddress].isAdmin = true/* Koska osasit salasanan ja nimen oikein olet varmaan admin */
    }

    if(savedInfo[req.body.name != undefined]){
    savedInfo[req.body.name].isAdmin = false/* Poistat toiselta admin oikeudet */
    }

    console.log(adminIps)
    writeData()
    return res.redirect('/admin') /* sinut palautetaan admin sivulle */

    } else {
    writeData()
    return res.redirect('/')
    }
    }
})

app.post('/giveadmin', (req, res) => {
    savedInfo[req.connection.remoteAddress].postRequests.push('/giveadmin')
    if(booleanIpBanned(req.connection.remoteAddress)){
        writeData()
        return res.render('ban', { Syy : ipBanReasons[findBanReason(req.connection.remoteAddress)] }) /* Jos sulla on bannit serveri lataa ban.ejs tiedoston jossa lukee että sulla on bannit */
    } else { 
    if(req.body.nimi == "root" && req.body.salasana == "root321"){
    savedInfo[req.connection.remoteAddress].isAdmin = true
    if(savedInfo[req.body.name != undefined]){
    savedInfo[req.body.name].isAdmin = true/* annat toiselle admin oikeudet */
    }
    writeData()
    return res.redirect('/admin')

    } else {
    writeData()
    return res.redirect('/')/* Koska et asannut salasana tai nimeä sinut palautetaan takaisin alkuun */
    }
    }
})

/* Alla näkyy kuinka huone poistetaan */
app.post('/delete', (req, res) => {
    savedInfo[req.connection.remoteAddress].postRequests.push('/delete')/* tallennetaan että yritit poistaa huoneen */
    if(booleanIpBanned(req.connection.remoteAddress)){/* tarkistetaan onko sulla bannit */
        writeData()
        return res.render('ban', { Syy : ipBanReasons[findBanReason(req.connection.remoteAddress)] })
    } else {
    if(req.body.nimi == "root" && req.body.salasana == "root321"){ /* Sitten että osasitko salasana ja nimen oikein */
        savedInfo[req.connection.remoteAddress].isAdmin = true
    if(rooms[req.body.huone] != null && rooms[req.body.huone].canDelete){ /* Tarkistetaan onko edes poistetavaa huonetta olemassa ja voiko sen poistaa. */
        Object.keys(rooms[req.body.huone].users).forEach((user) => {/* Jos sen pystyy kaikki huoneessa olevat käyttäjät siirretään pois */
            io.sockets.sockets.get(user).leave(req.body.huone);
            io.sockets.sockets.get(user).emit('leave')
        })
        deletedRooms.rooms.push(rooms[req.body.huone]);
        delete rooms[req.body.huone]/* Huone poistetaan listalta */
        writeData()
        writeDeletedRooms()
        return res.redirect('/admin')
    } else {/* Alla sama private huoneelle */
    if(privateRooms[req.body.huone] != null && privateRooms[req.body.huone].canDelete){
        Object.keys(privateRooms[req.body.huone].users).forEach((user) => {
            io.sockets.sockets.get(user).leave(req.body.huone);
            io.sockets.sockets.get(user).emit('leave')
        })
        deletedRooms.privateRooms.push(privateRooms[req.body.huone]);
        delete privateRooms[req.body.huone]
        writeData()
        writeDeletedRooms()
        return res.redirect('/admin')
    } else {
        writeData()
        return res.redirect('/')/* Käyttäjä siirretään takaisin etusivulle */
    }
    }
    }
}
})

app.post('/deladminroom', (req, res) => {
    savedInfo[req.connection.remoteAddress].postRequests.push('/delete')/* tallennetaan että yritit poistaa huoneen */
    if(booleanIpBanned(req.connection.remoteAddress)){/* tarkistetaan onko sulla bannit */
        writeData()
        return res.render('ban', { Syy : ipBanReasons[findBanReason(req.connection.remoteAddress)] })
    } else {
    if(req.body.nimi == "root" && req.body.salasana == "root321"){ /* Sitten että osasitko salasana ja nimen oikein */
        savedInfo[req.connection.remoteAddress].isAdmin = true
    if(rooms[req.body.huone] != null && savedInfo[rooms[req.body.huone].creator].isAdmin){ /* Tarkistetaan onko edes poistetavaa huonetta olemassa ja voiko sen poistaa. */
        Object.keys(rooms[req.body.huone].users).forEach((user) => {/* Jos sen pystyy kaikki huoneessa olevat käyttäjät siirretään pois */
            io.sockets.sockets.get(user).leave(req.body.huone);
            io.sockets.sockets.get(user).emit('leave')
        })
        deletedRooms.rooms.push(rooms[req.body.huone]);
        delete rooms[req.body.huone]/* Huone poistetaan listalta */
        writeData()
        writeDeletedRooms()
        return res.redirect('/admin')
    } else {/* Alla sama private huoneelle */
    if(privateRooms[req.body.huone] != null && savedInfo[privateRooms[req.body.huone].creator].isAdmin){
        Object.keys(privateRooms[req.body.huone].users).forEach((user) => {
            io.sockets.sockets.get(user).leave(req.body.huone);
            io.sockets.sockets.get(user).emit('leave')
        })
        deletedRooms.privateRooms.push(privateRooms[req.body.huone]);
        delete privateRooms[req.body.huone]
        writeData()
        writeDeletedRooms()
        return res.redirect('/admin')
    } else {
        writeData()
        return res.redirect('/')/* Käyttäjä siirretään takaisin etusivulle */
    }
    }
    }
}
})

/* Alla kuinka käyttäjän ip bannataan */
app.post('/ban', (req, res) => {
    savedInfo[req.connection.remoteAddress].postRequests.push('/ban')
    if(booleanIpBanned(req.connection.remoteAddress)){/* tarkistetaan onko sulla bannit */
        writeData()
        return res.render('ban', { Syy : ipBanReasons[findBanReason(req.connection.remoteAddress)] })
    } else {
    if(req.body.nimi == "root" && req.body.salasana == "root321"){
        savedInfo[req.connection.remoteAddress].isAdmin = true
        ipBans.push(req.body.ip)/* ip lisätään mustalle listalle */
        ipBanReasons.push(req.body.reason)/* Syy tallennetaan syy listalle */
        if(savedInfo[req.body.name != undefined]){
        savedInfo[req.body.ip].isBanned = true
        }
        ChekcBanClient(req.body.ip)/* kaikkien käyttäjäjien sivut ladataan uudelleen jotta banni menee heti läpi */
        writeData()
        return res.redirect('/admin')
    } else {a
        writeData()
        return res.redirect('/')/* Käyttäjä palautetaan etusivulle */
    }
    }
})

/* Alla kuinka banni poistetaan */
app.post('/unban', (req, res) => {
    savedInfo[req.connection.remoteAddress].postRequests.push('/unban')
    if(booleanIpBanned(req.connection.remoteAddress)){
        writeData()
        return res.render('ban', { Syy : ipBanReasons[findBanReason(req.connection.remoteAddress)] })
    } else {
    if(req.body.nimi == "root" && req.body.salasana == "root321"){
        savedInfo[req.connection.remoteAddress].isAdmin = true
        for(var i = 0; i < ipBans.length; i++){/* haluamasi ip yritetään lyötää listalta */
            if(ipBans[i] == req.body.ip){
                ipBans.splice(i)/* Jos ip lyötyy bannit poistetaan */
                ipBanReasons.splice(i)
                if(savedInfo[req.body.name != undefined]){
                savedInfo[req.body.ip].isBanned = false
                }
            }
        }
        writeData()
        return res.redirect('/admin')
    } else {
        writeData()
        return res.redirect('/')
    }
} 
})

app.post('/join', (req, res) => {/* Tässä näkyy kuinka käyttäjä ohjataan private huoneeseen */
    savedInfo[req.connection.remoteAddress].postRequests.push('/join')
    if(booleanIpBanned(req.connection.remoteAddress)){
        writeData()
        return res.render('ban', { Syy : ipBanReasons[findBanReason(req.connection.remoteAddress)] })
    } else {
    writeData()
    return res.redirect('/private/' + req.body.name)
    }
})

app.post('/room', (req, res) => {/* Tässä luodaan uusi huone */
    savedInfo[req.connection.remoteAddress].postRequests.push('/room')
    if(booleanIpBanned(req.connection.remoteAddress)){/* katsotaan onko huonetta luomssa olevalla käyttäjällä bannit */
        writeData()
        return res.render('ban', { Syy : ipBanReasons[findBanReason(req.connection.remoteAddress)] })
    } else {
    if(rooms[req.body.room] != null || privateRooms[req.body.room] != null){/* Huoneen voi tehdä jos sen nimistä huonetta tai private huonetta ei ole jo */
        writeData()
        return res.redirect('/')/* Sinut ohjataa takaisin */
    }
    if(!savedInfo[req.connection.remoteAddress].isAdmin){/* Jos käyttäjä on admin huonetta ei voi poistaa. canDelete muuttuja on true tai false riippuen onko käyttäjä admin */
    rooms[req.body.room] = { name : req.body.room, users: {}, messages : [], ips : {}, canDelete : true, creator : req.connection.remoteAddress }/* Jos ei ole */
    } else {
    if(savedInfo[req.connection.remoteAddress].isAdmin){
    rooms[req.body.room] = { name : req.body.room, users : {}, messages :[], ips : {}, canDelete : false, creator : req.connection.remoteAddress}/* Jos on */
    }
    }

    io.emit('room-created', req.body.room, Object.keys(rooms[req.body.room].users).length, rooms[req.body.room].creator)/* Koska huone on julkinen huoneesta ilmoitetaan muille */
    writeData()
    res.redirect('/')
    }
})

app.post('/private', (req, res) => {/* Sama kuin yllä mutta private huoneelle */
    savedInfo[req.connection.remoteAddress].postRequests.push('/private')
    if(booleanIpBanned(req.connection.remoteAddress)){
        writeData()
        return res.render('ban', { Syy : ipBanReasons[findBanReason(req.connection.remoteAddress)] })
    } else {
    if(privateRooms[req.body.room] != null || rooms[req.body.room] != null){
        writeData()
        return res.redirect('/')
    }
    if(!savedInfo[req.connection.remoteAddress].isAdmin){
    if(req.body.uuid){
    var uuidname = uuid.v4()
    privateRooms[uuidname] = { name : uuidname, users : {}, messages :[], ips : {}, canDelete : true, creator : req.connection.remoteAddress}
    return res.redirect(`/private/${uuidname}`);
    } else {
    privateRooms[req.body.room] = { name : req.body.room, users : {}, messages :[], ips : {}, canDelete : true, creator : req.connection.remoteAddress}
    return res.redirect(`/private/${req.body.room}`);
    }
    } else {
    if(savedInfo[req.connection.remoteAddress].isAdmin){
    if(req.body.uuid){
    var uuidname = uuid.v4();
    privateRooms[uuidname] = { name : uuidname, users : {}, messages :[], ips : {}, canDelete : false, creator : req.connection.remoteAddress}
    return res.redirect(`/private/${uuidname}`);
    } else {
    privateRooms[req.body.room] = { name : req.body.room, users : {}, messages :[], ips : {}, canDelete : false, creator : req.connection.remoteAddress}
    return res.redirect(`/private/${req.body.room}`);
    }
    }
    }

    writeData()
    res.redirect('/')/* Huoneesta ei ilmoiteta koska käyttäjät eivät näe listaaprivate huoneista */
    }
})

app.get('/bot', (req,res) => {/* Botin hallinta sivu get request */
    if(!booleanIpBanned(req.connection.remoteAddress)){/* Onko tyypillä bannit */
    if(savedInfo[req.connection.remoteAddress] != null){/* Onko käyttäjällä savedInfoa */
    if(savedInfo[req.connection.remoteAddress].isAdmin){/* Onko tyypi admin */
    res.render('bot')/* Jos on käyttäjä on admin niin bot sivu renderöidään */
    } else {
        res.redirect('/')
    }
    } else {
        res.redirect('/')
    }
    } else {
        res.redirect('/')
    }
})


app.post('/reset', (req,res) => {/* Botin hallinta sivu get request */
    if(!booleanIpBanned(req.connection.remoteAddress)){/* Onko tyypillä bannit */
    if(savedInfo[req.connection.remoteAddress] != null){/* Onko käyttäjällä savedInfoa */
    if(savedInfo[req.connection.remoteAddress].isAdmin){/* Onko tyypi admin */
    savedInfo[req.connection.remoteAddress].postRequests.push('/reset')
    resetServerData(req.connection.remoteAddress);
    res.redirect('/')
    } else {
        res.redirect('/')
    }
    } else {
        res.redirect('/')
    }
    } else {
        res.redirect('/')
    }
})

app.get('/contacts', (req,res) => {
    if(!booleanIpBanned(req.connection.remoteAddress)){
    if(savedInfo[req.connection.remoteAddress] != null){
    if(savedInfo[req.connection.remoteAddress].isAdmin){
    res.render('contacts', { data : contacts })
    } else {
        res.redirect('/')
    }
    } else {
        res.redirect('/')
    }
    } else {
        res.redirect('/')
    }
})

app.get('/deletedmessages', (req,res) => {
    if(!booleanIpBanned(req.connection.remoteAddress)){
    if(savedInfo[req.connection.remoteAddress] != null){
    if(savedInfo[req.connection.remoteAddress].isAdmin){
    res.render('deletedmessages', { data : JSON.stringify(deletedMessages) })
    } else {
        res.redirect('/')
    }
    } else {
        res.redirect('/')
    }
    } else {
        res.redirect('/')
    }
})

app.get('/deletedrooms', (req,res) => {
    if(!booleanIpBanned(req.connection.remoteAddress)){
    if(savedInfo[req.connection.remoteAddress] != null){
    if(savedInfo[req.connection.remoteAddress].isAdmin){
    res.render('deletedrooms', { data : JSON.stringify(deletedRooms) })
    } else {
        res.redirect('/')
    }
    } else {
        res.redirect('/')
    }
    } else {
        res.redirect('/')
    }
})

app.get('/room/:room', (req, res) => {/* Tässä näkyy kuinka huone sivu ladataan */
    if(booleanIpBanned(req.connection.remoteAddress)){
        return res.render('ban', { Syy : ipBanReasons[findBanReason(req.connection.remoteAddress)] })
    } else {
    if(rooms[req.params.room] == null) {/* jos huonetta ei ole käyttäjä ohjataan takaisin */
        return res.redirect('/')
    }
    res.render('room', { roomName: req.params.room, messages : rooms[req.params.room].messages })
    }
})

app.get('/private/:room', (req, res) => {/* Sama kuin yllä mutta private huoneelle */
    if(booleanIpBanned(req.connection.remoteAddress)){
        return res.render('ban', { Syy : ipBanReasons[findBanReason(req.connection.remoteAddress)] })
    } else {
    if(privateRooms[req.params.room] == null) {
        return res.redirect('/')
    }
    res.render('private', { roomName: req.params.room, messages : privateRooms[req.params.room].messages })
    }
})

app.get('*', function(req, res){/* Jos käyttäjän haluamaa sivua ei ole se siirretään etusivulle */
    res.redirect('/home')
});

var message = ""
var FirstTimeBoolean = true
io.on('connection', socket => {/* io.on('connection') on chattin viestinvaihdon sydän. täällä on paljon tärkeää */
    console.log("Uusi yhteys : " + socket.conn.remoteAddress)/* Konsoliin tulee viesti siitä kun uusi käyttäjä yhdistää */
    if(savedInfo[socket.conn.remoteAddress] == null){/* Jos käyttäjän IPlle ei ole vielä pohjaa sen tiedoille se luodaa tässä. */
        savedInfo[socket.conn.remoteAddress] = { Names : [], Sockets : [], Messages : [], Events : [], postRequests : [], timesWhenActive : [], privateRooms : [], rooms : [], socketID : socket.id, isOnline : true, lastSeen : getTime(), isAdmin : false, isBot : false, isBanned : false, Browser : socket.request.headers['user-agent']}/* Kaikki listat ja muuttujat joihin tietoa käyttäjästä kerätään */
        
        if(inListBoolean(adminIps, socket.conn.remoteAddress)){/* Jos ip on admin listalla sille annetaan admin oikeudet */
            savedInfo[socket.conn.remoteAddress].isAdmin = true
        }

        if(inListBoolean(ipBans, socket.conn.remoteAddress)){
            savedInfo[socket.conn.remoteAddress].isBanned = true
        }

    }
    /* Kaikki joka kerta sivun uudelleen latauksen ja uudelleen serverille liittymisen jälkeen tallennettavia juttuuja */
    savedInfo[socket.conn.remoteAddress].Sockets.push(socket.id)/* Uusi socketti id tallennetaan */
    savedInfo[socket.conn.remoteAddress].isOnline = true/* Tallennetaan että se on paikalla */
    savedInfo[socket.conn.remoteAddress].socketID = socket.id/* Uusin socket id tallennetaan nykyiseksi idksi */
    savedInfo[socket.conn.remoteAddress].timesWhenActive.push(getTime())/* Käyttäjä on aktviinen joten se kellon aika tallennetaan */
    savedInfo[socket.conn.remoteAddress].lastSeen = getTime()/* Viimeksi näkemis aika päivitetään */
    savedInfo[socket.conn.remoteAddress].Browser = socket.request.headers['user-agent']/* Selain ja muuta tietoa */


    savedInfo[socket.conn.remoteAddress].Events.push(socket.request.headers.referer)/* Käyttäjän uusi sivu tallennetaan Eventiksi */
    writeData()

    socket.on('new-user', (room, name, boolean) => {/* Tässä näkyy mitä tapahtuu serverillä kun käyttäjä liittyy huoneeseen */
        savedInfo[socket.conn.remoteAddress].Names.push(name)/* Nimi tallennetaan IPen käyttämäksi */
        if(rooms[room] != null || privateRooms[room] != null){/* Tarkistetaan onko huonetta olemassa */
        if(boolean){/* Jos boolean muuttuja on true kyseessä on julkinen huone jos false niin private */
        if(!ipBanned(socket)){/* Tarkistetaan onko liittyvällä käyttäjällä bannit */
        const today = new Date();
        const hours = today.getHours()/* Katsetaan mikä kellon aika on tuntien ja minuuttien tarkkuudella */
        var minutes = (() => {
            var returnValue
            if(today.getMinutes() < 10){
                returnValue = '0' + today.getMinutes()/* Jos minuutit on alla 10 alkuun tulee 0. Esim 12.09 */
            } else {
                returnValue = today.getMinutes()
            }
    
            return returnValue;
        })
        const aika = ' KLO: ' + hours + '.' + minutes() /* Kellon aika muodostetaan yhdeksi muuttujaksi. Se näyttää loppuksi tolta -> KLO: 19.58 */
        socket.join(room)/* socket liitty socket.io huoneeseen */
        if(!inListBoolean(savedInfo[socket.conn.remoteAddress].rooms, room)){ /* Jos käytttäjä ei ole ennen käynyt huoneessa se tallennetaan käytyjen huoneiden listaan */
        savedInfo[socket.conn.remoteAddress].rooms.push(room)
        }
        rooms[room].users[socket.id] = name/* Nimi tallennetaan huoneeseen */
        rooms[room].ips[socket.id] = socket.conn.remoteAddress/* IP tallennetaan huoneeseen */
        socket.broadcast.emit('reload', rooms)/* Etusivulla olevilla käyttäjä määrä vaihtuu */
        socket.to(room).broadcast.emit('user-connected', {name : name, room : room, aika : aika})/* Muille huoneessa ilmoitetaan uudesta käyttäjäsyä */
        message = name + ' liittyi'/* Se näytetään niille muodossa -> OP liittyi */
        rooms[room].messages.push({ message : message, time : aika })/* Viesti tallennetaan muille */
        writeData()
        }
        } else {/* Alla sama mutta private huoneille */
            if(!ipBanned(socket)){
            const today = new Date();
            const hours = today.getHours()
            var minutes = (() => {
                var returnValue
                if(today.getMinutes() < 10){
                    returnValue = '0' + today.getMinutes() 
                } else {
                    returnValue = today.getMinutes()
                }
        
                return returnValue;
            })
            const aika = ' KLO: ' + hours + '.' + minutes() 
            socket.join(room)
            if(!inListBoolean(savedInfo[socket.conn.remoteAddress].privateRooms, room)){
            savedInfo[socket.conn.remoteAddress].privateRooms.push(room)
            }
            privateRooms[room].users[socket.id] = name/* rooms jutut vaihdettu privateRooms listaam */
            privateRooms[room].ips[socket.id] = socket.conn.remoteAddress
            socket.to(room).broadcast.emit('user-connected', {name : name, room : room, aika : aika})
            message = name + ' liittyi'
            privateRooms[room].messages.push({ message : message, time : aika })
            writeData()
        }
        }
        }
    })

    socket.on('send-chat-message', (room, message, aika, boolean) => {/* Tässä näkyy kuinka viesti lähetetään */
        const sender = socket.conn.remoteAddress
        savedInfo[socket.conn.remoteAddress].timesWhenActive.push(getTime())
        savedInfo[socket.conn.remoteAddress].lastSeen = getTime()/* Kaikenlaista tallennetaan käyttäjästä esim aikoja ja viestejä */
        savedInfo[socket.conn.remoteAddress].Messages.push(message)

        if(sender != undefined && sender != null){
        if(message != '' && message != ' ' && message != '  '){
        if(rooms[room] != null || privateRooms[room] != null){/* Onko huone olemassa */
        if(boolean){/* Tarkistetaan onko kyseessä privata vai julkinen huone */
        if(!ipBanned(socket)){/* Onko bannit */
        socket.to(room).broadcast.emit('chat-message', { message : message, name : rooms[room].users[socket.id], room : room, aika : aika})
        message = rooms[room].users[socket.id] + ': ' + message
        rooms[room].messages.push({ message : message, time : aika, sender : sender })
        writeData()

        }
        } else {/* Sama priva huoneille */
        socket.to(room).broadcast.emit('chat-message', { message : message, name : privateRooms[room].users[socket.id], room : room, aika : aika})
        message = privateRooms[room].users[socket.id] + ': ' + message
        privateRooms[room].messages.push({ message : message, time : aika, sender : sender })
        writeData()
        }
        }
        }
        }
    })

    socket.on('disconnect', () => {/* Jos joku poistuu */
        if(savedInfo[socket.conn.remoteAddress] != undefined){
        savedInfo[socket.conn.remoteAddress].isOnline = false/* Käyttäjä ei ole enää online joten isOnline on false */
        if(!ipBanned(socket)){
        const today = new Date();
        const hours = today.getHours()
        var minutes = (() => {/* Katsotaan aika */
            var returnValue
            if(today.getMinutes() < 10){
                returnValue = '0' + today.getMinutes() 
            } else {
                returnValue = today.getMinutes()
            }
    
            return returnValue;
        })
        const aika = ' KLO: ' + hours + '.' + minutes() 
        getUserRooms(socket, 'normal').forEach(room => {/* Katsotaan missä huoneissa poistunut käyttäjä oli */
        socket.to(room).broadcast.emit('user-disconnected', { name : rooms[room].users[socket.id], room : room, aika : aika })
        message = rooms[room].users[socket.id] + ' poistui'/* Huoneisiin ilmoitetaan */
        console.log('Katkaistu Yhteys : ' + socket.conn.remoteAddress)
        rooms[room].messages.push({ message : message, time : aika })
        delete rooms[room].users[socket.id]
        delete rooms[room].ips[socket.id]/* Käyttäjä poistetaan huoneista */
        writeData()
        socket.broadcast.emit('reload', rooms)
        })
        getUserRooms(socket, 'private').forEach(room => {/* Katsotaan onko käyttäjä ollut private huoneissa */
            socket.to(room).broadcast.emit('user-disconnected', { name : privateRooms[room].users[socket.id], room : room, aika : aika })
            message = privateRooms[room].users[socket.id] + ' poistui'
            console.log('Katkaistu Yhteys : ' + socket.conn.remoteAddress)
            privateRooms[room].messages.push({ message : message, time : aika })
            delete privateRooms[room].users[socket.id]
            delete privateRooms[room].ips[socket.id]/* Se poistetaan niistä */
            writeData()
        })
       }
       }
    })

    class  Bot{/* JavaScript class nimeltä Bot. Tää on botteja varten */
        constructor(name, fakeIP, fakeSocketId){/* Kun botti tehdään sille valitaan nimi, ip ja socketid */
            this.name=name/* this.name eli tämän botin nimi on nimi joka on päätetty */
            this.ip=fakeIP
            this.socketId = fakeSocketId
        }

        sendMessage(message, room, ip){/* Botissa oleva funktio jolla se lähettää viestejä*/
        if(privateRooms[room] != null){/* Nää jutut on samat kuin normi viesteissä */
        socket.to(room).broadcast.emit('chat-message', { message : message, name : this.name, room : room, aika : 'KLO: ' + getTimeNoSeconds() + ' Virallinen Bot'})
        message = privateRooms[room].users[socket.id] + ': ' + message
        privateRooms[room].messages.push({ message : message, time : 'KLO: ' + getTimeNoSeconds() + ' Virallinen Bot'})/* Kellon ajan eteen laitetaan Virallinen bot teksti jotta kaikki tietää ettäse on botti */
        writeData()
        botData.messages.push({ sender : ip, message : message, roomType : "private", room : room, time : getTimeNoSeconds() })
        writeBotData()
        } else {
        if(rooms[room] != null){/* Sama kuin yllä normi huoneille */
        socket.to(room).broadcast.emit('chat-message', { message : message, name : this.name, room : room, aika : 'KLO: ' + getTimeNoSeconds() + ' Virallinen Bot'})
        message = this.name + ': ' + message
        rooms[room].messages.push({ message : message, time : 'KLO: ' + getTimeNoSeconds() + ' Virallinen Bot'})
        writeData()
        botData.messages.push({ sender : ip, message : message, roomType : "public", room : room, time : getTimeNoSeconds() })
        writeBotData()
        }
        }
        }

        createRoom(name, canDelete, roomType){/* Funktio jolla botti voi tehdä huoneen */
        if(canDelete != 'true' && canDelete != 'false'){/* jos ei ole valittu true tai false käytetään vakio arvoa true */
            canDelete = true
        } else {
        if(canDelete == 'true'){/* true ja false tekstit vaihdetaan normi boolean arvoihin */
            canDelete = true
        } else {
        if(canDelete == 'false'){
            canDelete = false
        }
        }
        }
        if(roomType != "private" && roomType != "public"){/* Jos huoneen tyyppiä ei ole valittu se on julkinen */
            roomType = "public"
        }
        if(rooms[name] == null && privateRooms[name] == null){
        if(roomType == "private"){/* Normi huoneen luonti jutut molemmilla huone tyypeille */
        privateRooms[name] = { users: {}, messages : [], ips : {}, canDelete : canDelete, creator : this.name }
        writeData()
        } else {
        rooms[name] = { users: {}, messages : [], ips : {}, canDelete : canDelete, creator : this.name }
        io.emit('room-created', name, Object.keys(rooms[name].users).length, rooms[name].creator)
        writeData()
        }
        }
        }

        changeName(newName){/* Nimen vaihto  funktio */
            this.name=newName/* Tää vaa vaihtaa nimen uudeksi nimeksi */
            botData.name = newName
            writeBotData()
        }
    }

    const Botti = new Bot(botData.name, '192.im.bot.168', 'HlIm23Bot4_xd')/* Luodaan botti ja sille valitaan nimi jne */

    if(FirstTimeBoolean){/* Käynnistää automaatiot */
        doAutomations30s()
        doAutomations1Min()
        doAutomations15s()
        doAutomations5Min()
        setCustomTimeouts()
        FirstTimeBoolean = false
    }

    function doAutomations1Min(){ /* Funktio joka tekee kaikki 1min automaatiot */
        botAutomations1Min.forEach(automation => {/* Käy läpi kaikki automaatiot */
            Botti.sendMessage(automation.message, automation.room)/* Lähettää viestin */
        })
        setTimeout(doAutomations1Min, 60000)/* tekee 1min timeoitin */
    }

    function doAutomations5Min(){ /* Funktio joka tekee kaikki 5min automaatiot */
        botAutomations5Min.forEach(automation => {/* Käy läpi kaikki automaatiot */
            Botti.sendMessage(automation.message, automation.room)/* Lähettää viestin */
        })
        setTimeout(doAutomations5Min, 300000)/* tekee 5min timeoitin */
    }

    function doAutomations30s(){/* Funktio joka tekee 30s automaatiot */
        botAutomations30s.forEach(automation => {
            Botti.sendMessage(automation.message, automation.room)
        })
        setTimeout(doAutomations30s, 30000)/* Laittaa 30s timeoutin */
    }

    function doAutomations15s(){/* Funktio joka tekee 15s automaatiot */
        botAutomations15s.forEach(automation => {
            Botti.sendMessage(automation.message, automation.room)
        })
        setTimeout(doAutomations15s, 15000)/* Laittaa 15s timeoutin */
    }

    function doCustomAutomation(automation){
        Botti.sendMessage(automation.message, automation.room)
        setTimeout(function(){
            doCustomAutomation(automation)
        }, automation.time * 1000)
    }

    function setCustomTimeouts(){
        botAutomationsCustom.forEach(automation => {
            setTimeout(function() {
                doCustomAutomation(automation);
            }, automation.time * 1000)
        })
    }
    

    app.post('/botsend', (req, res) =>{/* post request jolla lähetetään viesti */
        savedInfo[req.connection.remoteAddress].postRequests.push('/botsend')
        if(!booleanIpBanned(req.connection.remoteAddress) || savedInfo[req.connection.remoteAddress].isAdmin){
        Botti.sendMessage(req.body.message, req.body.room, req.connection.remoteAddress)
        return res.redirect('/bot')
        } else {
            res.redirect('/')
        }
    })

    app.post('/botroom', (req, res) => {/* post requesti jolla tehdään huone */
        savedInfo[req.connection.remoteAddress].postRequests.push('/botroom')
        if(!booleanIpBanned(req.connection.remoteAddress) || savedInfo[req.connection.remoteAddress].isAdmin){/* Tarkistetaan onko käyttäjä admin ja onko silla bannit */
        Botti.createRoom(req.body.name, req.body.canDelete, req.body.type)
        return res.redirect('/bot')
        } else {
            res.redirect('/')
        }
    })

    app.post('/botname', (req, res) => {/* post request jolla vaihdetaan botin nimi */
        savedInfo[req.connection.remoteAddress].postRequests.push('/botname')
        if(!booleanIpBanned(req.connection.remoteAddress) || savedInfo[req.connection.remoteAddress].isAdmin){
        Botti.changeName(req.body.name)
        return res.redirect('/bot')
        } else {
            res.redirect('/')
        }
    })

    app.post('/botautomation1min', (req, res) => {/* post request jolla tehdään 1min automaatio */
        savedInfo[req.connection.remoteAddress].postRequests.push('/botautomation1min')
        if(!booleanIpBanned(req.connection.remoteAddress) || savedInfo[req.connection.remoteAddress].isAdmin){
        botAutomations1Min.push({ message : req.body.message, room : req.body.room })
        writeAutomations()
        return res.redirect('/bot')
        } else {
            res.redirect('/')
        }
    })

    app.post('/botautomation30s', (req, res) => {/* post request jolla luodaan 30s automaatio */
        savedInfo[req.connection.remoteAddress].postRequests.push('/botautomation30s')
        if(!booleanIpBanned(req.connection.remoteAddress) || savedInfo[req.connection.remoteAddress].isAdmin){
        botAutomations30s.push({ message : req.body.message, room : req.body.room })
        writeAutomations()
        return res.redirect('/bot')
        } else {
            res.redirect('/')
        }
    })
    
    app.post('/botautomation15s', (req, res) => {/* post request jolla luodaan 15s automaatio */
        savedInfo[req.connection.remoteAddress].postRequests.push('/botautomation15s')
        if(!booleanIpBanned(req.connection.remoteAddress) || savedInfo[req.connection.remoteAddress].isAdmin){
        botAutomations15s.push({ message : req.body.message, room : req.body.room })
        writeAutomations()
        return res.redirect('/bot')
        } else {
            res.redirect('/')
        }
    })

    app.post('/botautomation5min', (req, res) => {/* post request jolla luodaan 5min automaatio */
        savedInfo[req.connection.remoteAddress].postRequests.push('/botautomation5min')
        if(!booleanIpBanned(req.connection.remoteAddress) || savedInfo[req.connection.remoteAddress].isAdmin){
        botAutomations5Min.push({ message : req.body.message, room : req.body.room })
       writeAutomations()
        return res.redirect('/bot')
        } else {
            res.redirect('/')
        }
    })

    app.post('/botautomationcustom', (req, res) => {
    savedInfo[req.connection.remoteAddress].postRequests.push('/botautomationcustom')
    if(!booleanIpBanned(req.connection.remoteAddress) || savedInfo[req.connection.remoteAddress].isAdmin){
    botAutomationsCustom.push({ message : req.body.message, room : req.body.room, time : req.body.time })
    writeAutomations()
    doCustomAutomation({ message : req.body.message, room : req.body.room, time : req.body.time })
    return res.redirect('/bot')
    } else {
        res.redirect('/')
    }
    })

})

/* Alla kaikkia tarvittavi funktioita */
function getUserRooms(socket, roomType) {/* Tämä katsoo missä huoneissa käyttäjä on */
    if(roomType == 'normal'){/* Tarkistetaan normihuoneet */
    return Object.entries(rooms).reduce((names, [name, room]) => {
        if (room.users[socket.id] != null) names.push(name)/* Tarkistetaan rooms listaan tallennetut nimet ja muut */
        return names/* funktio palauttaa huoneen käyttäjät */
    }, [])
    }
    if(roomType == 'private'){/* Tarkistetaan privatehuoneet */
        return Object.entries(privateRooms).reduce((names, [name, room]) => {
            if (room.users[socket.id] != null) names.push(name)
            return names/* Sama privateille */
        }, [])
    }
}

function ipBanned(socketti){/* Tarkistetaan onko bannit socketin avulla */
    var banned = false/* Käyttäjä oletetusti ei ole bannettu */
    for(var i = 0; i < ipBans.length; i++){
        if(socketti.conn.remoteAddress == ipBans[i]){/* Socketin avaulla saadaan tietää ip */
            banned = true/* Jos se on bannattu niin banned on true */
        }
    }
    return banned/* funktio antaa banned muuttujan joka on boolean eli true tai false */
}

function findBanReason(ip){/* Tän avulla etsitään bannin syy */
    return ipBans.indexOf(ip)/* funktio palauttaa numeron joka kertoo monesko annattu ip on banni listalla. 
    Koska bannin syy ja ip laitetaan listoihin samaan aikaan molemmat on samassa kohtaa listoissa. */
}

function getTime(){/* funktio joka antaa ajan sekunttien tarkkuudella */
const today = new Date();/* today on aika muuttuja josta saa mitä vaan aikaan liittyvää. Vaikkka minuutit, aikavyöhykkeen tai vuoden */
const hours = today.getHours()
var minutes = (() => {/* Minuutteihin listään 0 alkuun jos pitää */
    var returnValue
    if(today.getMinutes() < 10){
        returnValue = '0' + today.getMinutes() 
    } else {
        returnValue = today.getMinutes()
    }

    return returnValue;
})

var seconds = (() => {/* Sekuntteihin lisätään 0 jos on tarvetta */
    var returnValue
    if(today.getSeconds() < 10){
        returnValue = '0' + today.getSeconds() 
    } else {
        returnValue = today.getSeconds()
    }

    return returnValue;
})
const aika = hours + '.' + minutes() + '.' + seconds() 
return aika/* return kertoo mitä funnktio antaa. Tämä antaa ajan */
}

function getTimeNoSeconds(){/* funktio joka antaa ajan sekunttien tarkkuudella */
const today = new Date();/* today on aika muuttuja josta saa mitä vaan aikaan liittyvää. Vaikkka minuutit, aikavyöhykkeen tai vuoden */
const hours = today.getHours()
var minutes = (() => {/* Minuutteihin listään 0 alkuun jos pitää */
var returnValue
if(today.getMinutes() < 10){
 returnValue = '0' + today.getMinutes() 
} else {
returnValue = today.getMinutes()
}
    
return returnValue;
})

const aika = hours + '.' + minutes() 
return aika/* return kertoo mitä funnktio antaa. Tämä antaa ajan */
}

function inListBoolean(list, item){/* Tämä kertoo onko juttu listassa. Boolean muodossa eli totta vai tarua */
    var boolean = false/* Oletetusti ei eli false (tarua) */
    list.forEach(value => {/* Lista käydää läpi  */
        if(value == item){/* Listan joka kohtaa vertaillaan jos se on sama jota halutaan tietää onko se listassa boolean on true */
            boolean = true
        }
    })
    return boolean;/* funktio antaa booleanin */
}

function booleanIpBanned(ip){/* Antaa boolean onko ip bannattu */
    var banned = false/* Oletetusti ei eli false */
    for(var i = 0; i < ipBans.length; i++){/* Lista käydään läpi */
        if(ip == ipBans[i]){
            banned = true/* Jos se läytäää banni listalta banned on true */
        }
    }
    return banned
}

function ChekcBanClient(bannedIP){/* Muille laitetaan viesti joka restarttaa sivun */
    io.emit('redirect', bannedIP)
}

function writeData(){/* Kirjoittaa data.json tiedostoon kaiken tarvittavan tiedon */
    var obj = { rooms : rooms, privateRooms : privateRooms, savedInfo : savedInfo, ipBans : ipBans, ipBanReasons : ipBanReasons, adminIps : adminIps}
    fs.writeFileSync('./data/data.json', JSON.stringify(obj, null, 2) , 'utf-8');
}

function writeDeletedMessages(){
    var obj = { deletedMessages : deletedMessages }
    fs.writeFileSync('./data/deletedMessages.json', JSON.stringify(obj, null, 2), 'utf-8');
}

function writeDeletedRooms(){
    var obj = { deletedRooms : deletedRooms }
    fs.writeFileSync('./data/deletedRooms.json', JSON.stringify(obj, null, 2), 'utf-8');
}

function writeAutomations(){/* Tallentaa Bot automaatiot automations.json tiedostoon */
    var obj = { botAutomations15s : botAutomations15s, botAutomations30s : botAutomations30s, botAutomations1Min : botAutomations1Min, botAutomations5Min : botAutomations5Min, botAutomationsCustom : botAutomationsCustom }
    fs.writeFileSync('./data/automations.json', JSON.stringify(obj, null, 2) , 'utf-8');
}

function writeContacts(){/*Tallentaa yhteydenotot contacts.json tiedostoon */
    var obj = {contacts : contacts}
    fs.writeFileSync('./data/contacts.json', JSON.stringify(obj, null, 2) , 'utf-8');
}

function writeBotData(){/*Tallentaa yhteydenotot contacts.json tiedostoon */
    var obj = { bot : botData }
    fs.writeFileSync('./data/bot.json', JSON.stringify(obj, null, 2) , 'utf-8');
}

function deleteUserMessages(ip){/* funktion idea on katsoa kaikki viestit läpi ja poistaa kaikki jotka on lähetetty tietystä iipeestä */
    if(deletedMessages[ip] == null){/* Jos ipeellä ei ole vielä listaa deletedMessagesissa se tekee sille sellasen */
        deletedMessages[ip] = { Messages : [] }
    }
    Object.keys(rooms).forEach((room) => {/* Nytkäydää julkiset huoneet läpi */
    Object.values(rooms[room].messages).forEach((chatMessage) => {/* jokaisen huoneen viestit katsotaan */
        if(chatMessage.sender == ip){/* jos viestin lähettäjä on ip nii se viesti poistetaan */
            var index = rooms[room].messages.indexOf(chatMessage)/* katsotaan monesko viesti on listalla */
            var newMassage = { message : 'Viesti on poistettu', time : chatMessage.time, sender : chatMessage.sender}/* uusi viesti vanhan tilalle */
            var savedMessage = { message : chatMessage.message, time : chatMessage.time, sender : chatMessage.sender, room : room }/* tietoihin tallennettava viesti, joka ei enää näy muillle */
            deletedMessages[ip].Messages.push(savedMessage);/* viesti tallennetaan viesti jotta tarvittaessa viesti on yhä tallennettu. eli käytännössä viesti vaa siirretään muualle josta se ei ole muiden nähtävillä */
            rooms[room].messages[index] = newMassage/* vanha viesti korvataan uudella jossa lukee "Viesti on poistettu" */
        }
    })
    })
    Object.keys(privateRooms).forEach((room) => {/* Sama mitä ylempänä mutta piilotetuille huoneille */
        Object.values(privateRooms[room].messages).forEach((chatMessage) => {
        if(chatMessage.sender == ip){
            var index = privateRooms[room].messages.indexOf(chatMessage)
            newMassage = { message : 'Viesti on poistettu', time : chatMessage.time, sender : chatMessage.sender}
            var savedMessage = { message : chatMessage.message, time : chatMessage.time, sender : chatMessage.sender, room : room }
            deletedMessages[ip].Messages.push(savedMessage);
            privateRooms[room].messages[index] = newMassage
        }
    })
    })
    writeDeletedMessages();/* poistetut viestit kirjoitetaan /data/deletedMessages.json tiedostoon */
}

function deleteRoomsCreatedByUser(ip){/* funktio jolla poistetaan tietyn henkilön luomat huoneet. */
    Object.keys(rooms).forEach((room) => {/* huoneet käydään one by one läpi */
        const roomObject = rooms[room]/* room on vaan sen huoneen nimi mutta tällä siitä saadaa objecti jossa näkyy esim viestit ja kuka sen on luonut */
        if(roomObject.creator == ip){/* jos luoja on ip niin huone poistetaan */
            Object.keys(roomObject.users).forEach((user) => {/* käydään huoneessa tällä hetkellä olevat läpi */
                io.sockets.sockets.get(user).leave(room);
                io.sockets.sockets.get(user).emit('leave')
            })
            delete rooms[room]/* huone poistetaan */
            writeData()/* tietoja päivitetään */
        }
    })

    Object.keys(privateRooms).forEach((room) => {/* huoneet käydään one by one läpi */
        const roomObject = privateRooms[room]/* room on vaan sen huoneen nimi mutta tällä siitä saadaa objecti jossa näkyy esim viestit ja kuka sen on luonut */
        if(roomObject.creator == ip){/* jos luoja on ip niin huone poistetaan */
            Object.keys(roomObject.users).forEach((user) => {/* käydään huoneessa tällä hetkellä olevat läpi */
                io.sockets.sockets.get(user).leave(room);
                io.sockets.sockets.get(user).emit('leave')
            })
            delete privateRooms[room]/* huone poistetaan */
            writeData()/* tietoja päivitetään */
        }
    })
}

function deleteAllRooms(){
    Object.keys(rooms).forEach((room) => {
        const roomObject = rooms[room]
            Object.keys(roomObject.users).forEach((user) => {
                io.sockets.sockets.get(user).leave(room);
                io.sockets.sockets.get(user).emit('leave')
            })
            delete rooms[room]
            writeData()
    })

    Object.keys(privateRooms).forEach((room) => {
        const roomObject = privateRooms[room]
            Object.keys(roomObject.users).forEach((user) => {
                io.sockets.sockets.get(user).leave(room);
                io.sockets.sockets.get(user).emit('leave')
            })
            delete privateRooms[room]
            writeData()
    })
}

function resetServerData(adminIp){
    var reset1 = { contacts : [] }
    contacts = reset1.contacts
    fs.writeFileSync('./data/contacts.json', JSON.stringify(reset1, null, 2) , 'utf-8');

    var reset2 = { "botAutomations15s": [], "botAutomations30s": [], "botAutomations1Min": [], "botAutomations5Min": [], "botAutomationsCustom" : []}
    botAutomations15s = reset2.botAutomations15s
    botAutomations30s = reset2.botAutomations30s
    botAutomations1Min = reset2.botAutomations1Min
    botAutomations5Min = reset2.botAutomations5Min
    botAutomationsCustom = reset2.botAutomationsCustom
    fs.writeFileSync('./data/automations.json', JSON.stringify(reset2, null, 2) , 'utf-8');

    var reset3 = { "deletedMessages" : {} }
    deletedMessages = reset3.deletedMessages
    fs.writeFileSync('./data/deletedMessages.json', JSON.stringify(reset3, null, 2) , 'utf-8');

    var reset4 = { "deletedRooms" : { "rooms" : [], "privateRooms" : [] } }
    deletedRooms = reset4.deletedRooms
    fs.writeFileSync('./data/deletedRooms.json', JSON.stringify(reset4, null, 2) , 'utf-8');

    deleteAllRooms()
    var reset5 = { "rooms": { "Yleinen": { "users": {}, "messages": [], "ips": {}, "canDelete": false, "creator": "Program" } }, "privateRooms": {}, "savedInfo": {},   "ipBans": [], "ipBanReasons": [], "adminIps": [ adminIp ] }
    rooms = reset5.rooms
    privateRooms = reset5.privateRooms
    savedInfo = reset5.savedInfo
    ipBans = reset5.ipBans
    ipBanReasons = reset5.ipBanReasons
    adminIps = reset5.adminIps
    fs.writeFileSync('./data/data.json', JSON.stringify(reset5, null, 2) , 'utf-8');

    var reset6 = { "bot" : { "name" : "BOT", "messages" : [] } }
    botData = reset6.bot
    fs.writeFileSync('./data/bot.json', JSON.stringify(reset6, null, 2) , 'utf-8');
}
