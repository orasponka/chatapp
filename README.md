# ChatApp

ChatApp is simple nodejs real-time web chatting application. <br />
There is currently only one language supported and it is Finnish.

Based on Web Dev Simplified's [chat project](https://github.com/WebDevSimplified/Realtime-Chat-App-With-Rooms) and [youtube tutorial](https://www.youtube.com/watch?v=UymGJnv-WsE&ab)
but most of code is written by me. For example in original server.js is 58 lines long and in my version server.js is 1000+ lines long. <br> <br>
This project is about 3 years old and that's is reason that some code is miscellaneous and beginner level but anyway it works. <br>
Therefore also entire history of the ChatApp project is not aviable here.

## Features
**Real-time chatting** : Chat with small delay. <br />
**Public and private rooms** : Public rooms are shown in homepage and private rooms are not. <br />
**Admin system** : Admins can delete rooms, ban users, etc. <br />
**Contact page** : Users can send feedback to admins. <br />
**Responsive design**, looks nice on every device. <br />

## Requirments
**Node modules**: http, express, socket.io, path, fs, cors, ejs, uuid <br />
**Sudo access** required for port 80. <br />
**Nodejs and npm** must be installed on your system.

## How to run ChatApp? 
### Linux:
Run commands below.
```
git clone https://gitlab.com/orasponka/chatapp.git
cd chatapp/ChatApp/
chmod +x ./install.sh
./install.sh
sudo node server.js
```
## How to install ChatApp to your system?
### Install using pacman (Arch Linux)
If you have enabled my [op-arch-repo](https://gitlab.com/orasponka/op-arch-repo) you can install ChatApp via pacman
```
sudo pacman -Sy chatapp
```
And now you can run ChatApp via chatapp command
### Install using wget (Arch Linux)
```
wget "https://gitlab.com/orasponka/chatapp/-/raw/main/packages/chatapp.pkg.tar.zst"
sudo pacman -U chatapp.pkg.tar.zst
```
### Install using wget (Debian/Ubuntu)
```
wget "https://gitlab.com/orasponka/chatapp/-/raw/main/packages/chatapp.deb"
sudo dpkg -i chatapp.deb
```
## How to build ChatApp package?
### Arch Linux
Build ChatApp package (.pkg.tar.zst) by doing things below. Make sure you have a base-devel installed.
```
git clone https://gitlab.com/orasponka/chatapp.git
cd chatApp/arch_build/
chmod +x ./generatezip.sh
./generatezip.sh
makepkg
sudo pacman -U *.zst
```
### Debian/Ubuntu
Build ChatApp package (.deb) by doing things below.
```
git clone https://gitlab.com/orasponka/chatapp.git
cd chatApp/debian_build/
chmod +x ./generatefiles.sh
chmod +x DEBIAN/post*
./generatefiles.sh
dpkg-deb --build chatapp
sudo dpkg -i chatapp.deb
```
And now you can run ChatApp via chatapp command
## Gallery
### Main menu
![](https://gitlab.com/orasponka/chatapp/-/raw/main/screenshots/chatapp-1.png)
### Chat view
![](https://gitlab.com/orasponka/chatapp/-/raw/main/screenshots/chatapp-2.png)
### My data page
![](https://gitlab.com/orasponka/chatapp/-/raw/main/screenshots/chatapp-3.png)
