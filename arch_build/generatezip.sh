#!/usr/bin/env bash

pwd=$(pwd)

cd ./../ChatApp
zip -r chatapp.zip *
cp chatapp.zip $pwd
rm chatapp.zip
cd $pwd

