#!/usr/bin/env bash

mkdir -p chatapp/usr/share/chatapp/
mkdir -p chatapp/usr/bin/
mkdir -p chatapp/usr/share/licenses/chatapp/

cp LICENSE chatapp/usr/share/licenses/chatapp/
cp chatapp.sh chatapp/usr/bin/chatapp
cp -r ./../ChatApp/* chatapp/usr/share/chatapp/
cp -r DEBIAN chatapp/
